const inputContainer = getElement('#input-container');
const countdownForm = getElement('#countdown-form');
const titleElm = getElement('#title');
const dateElm = getElement('#date-picker');
const countdownElm = getElement('#countdown');
const countdownTitleElm = getElement('#countdown-title');
const countdownBtn = getElement('#countdown-button');
const timeElms = document.querySelectorAll('span');
const completeElm = getElement('#complete');
const completeInfoElm = getElement('#complete-info');
const completeBtn = getElement('#complete-button');

let countdownTitle = '';
let countdownDate = '';
let coundownValue = new Date();
let countdownActive;
dateElm.value = '';

const second = 1000;
const minute = second * 60;
const hour = minute * 60;
const day = hour * 24;

function getElement(selector) {
	return document.querySelector(selector);
}

// Set date input min with today's date
const today = new Date().toISOString().split('T')[0];
let [year, month, date] = today.split('-');

// Date picker module
const datepicker = new Datepicker(dateElm, {
	autohide: true,
	clearBtn: true,
	disableTouchKeyboard: true,
	minDate: `${month}/${date}/${year}`,
});

dateElm.setAttribute('min', today);

// Populate countdown / complete UI
function updateDOM() {
	countdownActive = setInterval(() => {
		const now = new Date().getTime();
		const distance = coundownValue - now;

		const days = Math.floor(distance / day);
		const hours = Math.floor((distance % day) / hour);
		const minutes = Math.floor((distance % hour) / minute);
		const seconds = Math.floor((distance % minute) / second);

		// Hide input
		inputContainer.hidden = true;

		// If the coundown has ended, show completed
		if (distance < 0) {
			countdownElm.hidden = true;
			clearInterval(countdownActive);
			completeInfoElm.textContent = `${countdownTitle} finished on ${countdownDate}`;
			completeElm.hidden = false;
		} else {
			// Else, show the countdown in progress
			countdownTitleElm.textContent = `${countdownTitle}`;
			timeElms[0].textContent = `${days}`;
			timeElms[1].textContent = `${hours}`;
			timeElms[2].textContent = `${minutes}`;
			timeElms[3].textContent = `${seconds}`;
			completeElm.hidden = true;
			countdownElm.hidden = false;
		}
	}, second);
}

// Take values from form input
function updateCountdown(e) {
	e.preventDefault();
	countdownTitle = e.srcElement[0].value;
	countdownDate = e.srcElement[1].value;

	// Create object for localStorage
	const savedCountdown = {
		title: countdownTitle,
		date: countdownDate,
	};

	localStorage.setItem('countdown', JSON.stringify(savedCountdown));

	// Check for valid date
	if (countdownDate === '') {
		alert('Please select a date for the countdown');
	} else {
		// Get number version of current date, updateDOM
		coundownValue = new Date(countdownDate).getTime();
		updateDOM();
	}
}

// Reset timer
function reset() {
	// Hide countdowns, show input
	countdownElm.hidden = true;
	completeElm.hidden = true;
	inputContainer.hidden = false;

	// Stop the countdown
	clearInterval(countdownActive);

	// Reset input values
	titleElm.value = '';
	dateElm.value = '';
	countdownTitle = '';
	countdownDate = '';
	localStorage.removeItem('countdown');
}

function restorePrevCountdown() {
	// Get countdown from localStorage if available
	if (localStorage.getItem('countdown')) {
		inputContainer.hidden = true;
		const { title, date } = JSON.parse(localStorage.getItem('countdown'));
		countdownTitle = title;
		countdownDate = date;
		coundownValue = new Date(countdownDate).getTime();
		updateDOM();
	}
}

// Event listeners
countdownForm.addEventListener('submit', updateCountdown);
countdownBtn.addEventListener('click', reset);
completeBtn.addEventListener('click', reset);

// On load, check localStorage
restorePrevCountdown();
